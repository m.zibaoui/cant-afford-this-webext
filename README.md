## About

This extension allows you to set a maximum price for shopping things on the internet.
If the item is above the price, and overlay will be shown over the page letting you know that you can't afford this.  


### Motivations

While this extension has some (arguable) value for end users, the main goal is for the author to learn about a few technologies and experiment. Namely I am interested in better understanding:

- [Manifest V3](https://developer.chrome.com/docs/extensions/develop/migrate/what-is-mv3) and web extensions in general
- [The Plasmo framework](https://docs.plasmo.com/framework)
- [pnpm](https://pnpm.io/)
- Automating deployments to Chrome Web Store, Mozilla's AMO and other browser web extension stores.

## Building the extension

Requires node 18 and pnpm

```bash
pnpm build --zip # Chrome MV3
```

```bash
npm build --zip --target=firefox-mv3 # Firefox MV3
```

## Development

This is a [Plasmo extension](https://docs.plasmo.com/) project bootstrapped with [`plasmo init`](https://www.npmjs.com/package/plasmo).

**Make sure to [install pnpm](https://pnpm.io/installation) first**.

Then:
```bash
pnpm dev
```
