import browser from "webextension-polyfill";



import { Storage } from "@plasmohq/storage";

export {};

browser.runtime.onMessage.addListener(async (message) => {
  if (message.type !== "tabActivated") {
    return;
  }

  const maxPrice = await storage.get("maxPrice");
  checkPage(maxPrice);
});

async function checkPage(maxPrice) {
  const currentUrl = window.location.href;

  const isAmazonUrl = currentUrl.includes("amazon.com");
  const microDataTag = document.querySelector(
    "script[type='application/ld+json']",
  );

  if (!isAmazonUrl && !microDataTag) {
    storage.set("showOverlay", false);
    return;
  }

  if (isAmazonUrl) {
    // AmazonLogic
  }

  if (microDataTag) {
    let microData;

    try {
      microData = JSON.parse(microDataTag.innerHTML);
    } catch (e) {
      console.error("could not parse micro data");
      console.error(e);
      storage.set("showOverlay", false);
      return;
    }

    if (microData["@type"] !== "Product") {
      storage.set("showOverlay", false);
      return;
    }

    const price = microData.offers?.price;
    if (!price) {
      storage.set("showOverlay", false);
      return;
    }

    storage.set("showOverlay", price > maxPrice);
  }
}

const storage = new Storage();
storage.watch({
  maxPrice: (mp) => {
    checkPage(mp.newValue);
  },
});

(async () => {
  const maxPrice = await storage.get("maxPrice");
  checkPage(maxPrice);
})();
