import { useStorage } from "@plasmohq/storage/hook";

const Overlay = () => {
  const [showOverlay] = useStorage("showOverlay");
  if (!showOverlay) {
    return null;
  }

  return (
    <aside
      style={{
        position: "fixed",
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        background: "rgba(255, 255, 255, 0.7)",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
      }}>
        <div style={{
          padding: "2em 1em",
          border: "1px solid black",
          borderRadius: "5px",
          background: "white",
          fontSize: "7vw",
        }}>You can't afford this 😔</div>
    </aside>
  );
};

export default Overlay;
