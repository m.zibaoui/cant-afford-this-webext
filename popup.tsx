import { useStorage } from "@plasmohq/storage/hook";

function IndexPopup() {
  const [maxPrice, setMaxPrice] = useStorage("maxPrice");

  return (
    <div
      style={{
        padding: 16,
      }}>
      <h2>Set your max price (EUR)</h2>
      <input
        type="number"
        onChange={(e) => setMaxPrice(e.target.value)}
        value={maxPrice}
      />
    </div>
  );
}

export default IndexPopup;
